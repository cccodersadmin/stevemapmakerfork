from django.contrib import admin
from django.conf import settings
from django.conf.urls import patterns, url, include
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from rest_framework.urlpatterns import format_suffix_patterns
from map_server import views

# Gross hack to serve under directory, not subdomain
prefix = settings.URL_PREFIX


urlpatterns = patterns('',
    url(r'^' + prefix + '$', views.UserSubmissionList.as_view(), name='moderation-home'),
    url(r'^' + prefix + '(?P<map_id>[0-9]+)/$', views.UserSubmissionList.as_view(), name='moderation-map'),
    url(r'^' + prefix + '(?P<map_id>[0-9]+)/approved_submissions.json$', views.UserSubmissionListPublic.as_view(), name='approved_submissions_feed'),
    # url(r'^' + prefix + 'plain/$', views.UserSubmissionListPublic.as_view(), name='usersubmission_feed'),

    url(r'^' + prefix + 'plain/post/$', views.UserSubmissionListPost.as_view(), name='user_submission_post'),
    url(r'^' + prefix + 'submission/(?P<pk>[0-9]+)/$', views.UserSubmissionDetail.as_view(), name='submission-detail'),

    url(r'^' + prefix + 'login/$', 'django.contrib.auth.views.login'),
    url(r'^' + prefix + 'logout/$', 'django.contrib.auth.views.logout', {'next_page': '/maps/'}),

    url(r'^' + prefix + 'mapmaker/maps/?$', views.MapList.as_view(), name='mapmaker-map-list'),
    url(r'^' + prefix + 'mapmaker/map/(?P<pk>[0-9]+)/?$', views.MapDetail.as_view(), name='mapmaker-map-detail'),
    url(r'^' + prefix + 'mapmaker/layers/?$', views.LayerList.as_view(), name='mapmaker-layer-list'),
    url(r'^' + prefix + 'mapmaker/layer/(?P<pk>[0-9]+)/?$', views.LayerDetail.as_view(), name='mapmaker-layer-detail'),
    url(r'^' + prefix + 'mapmaker/datasets/?$', views.LayerDatasetList.as_view(), name='mapmaker-dataset-list'),
    url(r'^' + prefix + 'mapmaker/dataset/(?P<pk>[0-9]+)/?$', views.LayerDatasetDetail.as_view(), name='mapmaker-dataset-detail'),
    url(r'^' + prefix + 'mapmaker/markericons/?$', views.MarkerIconList.as_view(), name='mapmaker-markericon-list'),
    url(r'^' + prefix + 'mapmaker/markericon/(?P<pk>[0-9]+)/?$', views.MarkerIconDetail.as_view(), name='mapmaker-markericon-detail'),

    url(r'^' + prefix + 'mapmaker/map/(?P<pk>[0-9]+)/preview/?$', views.MapPreview.as_view(), name='map-preview'),
    url(r'^' + prefix + 'mapmaker/map/(?P<pk>[0-9]+)/publish/?$', views.MapPublish.as_view(), name='map-publish'),
)


urlpatterns = format_suffix_patterns(urlpatterns)
urlpatterns += patterns('',
    url(r'^' + prefix + 'auth/', include('rest_framework.urls', namespace='rest_framework')),
)


urlpatterns += patterns('',
    url(r'^' + prefix + 'user_submission_email_addresses.csv$', views.user_submission_email_addresses, name='email-addresses-csv'),
    url(r'^' + prefix + 'utils/convert-en-to-latlng/$', login_required(views.ConvertEnToLatLng.as_view()), name='convert-en-to-latlng'),
    url(r'^' + prefix + 'utils/bulk-geocode/$', login_required(views.BulkGeocode.as_view()), name='bulk-geocode'),
)

urlpatterns += patterns('',
    url(r'^' + prefix + 'mapmaker/$', login_required(TemplateView.as_view(template_name="map_maker/index.html")), name='mapmaker-home'),
    url(r'^' + prefix + 'mapmaker/controller.js$', TemplateView.as_view(template_name="map_maker/controller.js"), name='mapmaker-controller'),
    url(r'^' + prefix + 'mapmaker/partial/map.html$', TemplateView.as_view(template_name="map_maker/partials/map.html"), name='mapmaker-partial-map'),
    url(r'^' + prefix + 'mapmaker/partial/header.html$', TemplateView.as_view(template_name="map_maker/partials/header.html"), name='mapmaker-partial-header'),
    url(r'^' + prefix + 'mapmaker/partial/detail.html$', TemplateView.as_view(template_name="map_maker/partials/detail.html"), name='mapmaker-partial-detail'),
    url(r'^' + prefix + 'mapmaker/partial/empty.html$', TemplateView.as_view(template_name="map_maker/partials/empty.html"), name='mapmaker-partial-empty'),
)


admin.autodiscover()
urlpatterns += patterns('',
    (r'^' + prefix + 'admin/', include(admin.site.urls)),
)


# Must serve static assets with a _real_ web server. This is not suitable for production!
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^' + prefix + 'files/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        url(r'^' + prefix + 'static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
        url(r'^' + prefix + 'maps/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MAP_PUBLISH_DIR, 'show_indexes': True}, name='frontend-maps'),
        url(r'^' + prefix + 'feeds/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.FEED_OUTPUT_DIR, 'show_indexes': True}, name='feeds'),
    )

urlpatterns += patterns('',
    url(r'^' + prefix + 'maps/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MAP_PUBLISH_DIR, 'show_indexes': True}, name='frontend-maps'),
    url(r'^' + prefix + 'feeds/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.FEED_OUTPUT_DIR, 'show_indexes': True}, name='feeds'),
)
