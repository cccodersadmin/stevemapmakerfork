#!/usr/bin/env python
import os
import sys

from os.path import abspath, dirname, join

sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(abspath(join(dirname(__file__), "../env/lib/python2.7/site-packages/")))

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "map_server.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
